
//#include "C:\Users\tomro\Desktop\VisualStudioProject\UnitTest\UnitTestGtestLib\gtest\gtest.h"
#include "gtest.h"
#include "Math.h"

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
class MathTest : public ::testing::Test {
protected:
	Math* math_;

	MathTest() {
		// You can do set-up work for each test here.
	}
	virtual ~MathTest() {
		// You can do clean-up work that doesn't throw exceptions here.
	}

	virtual void SetUp();      // Code here will be called immediately after the constructor (right
                               // before each test).
	virtual void TearDown();   // Code here will be called immediately after each test (right
                               // before the destructor).

			/* Stuff to test Section */

	// Addtwo numbers
	int addTwoNum(int number1, int number2);
	// Sub two numbers
	int subTwoNum(int number1, int number2);
	// Div two numbers
	int divTwoNum(int number1, int number2);
	// Multi two numbers
	int mulTwoNum(int number1, int number2);
	// Factorial
	int factNum(int number1);
};