
#include "Math.h"

int Math::addTwoNum(int number1, int number2)
{
	return (number1 + number2);
}

int Math::subTwoNum(int number1, int number2)
{
	return (number1 - number2);
}

int Math::mulTwoNum(int number1, int number2)
{
	return (number1 * number2);
}

int Math::divTwoNum(int number1, int number2)
{
	return (number1 / number2);
}

int Math::factNum(int number1)
{
	if (number1 == 0)
		return 1;
	else
		return(number1 * factNum(number1 - 1));
}

/*
void Math::setNum(int number1, int number2) {
	num1 = number1;
	num2 = number2;
}

int Math::getNum1(void) {

	return num1;
}

int Math::getNum2(void) {

	return num2;
}
*/

// Traditional "C" approach 
#if C_APPROACH
int factNum(int num1)
{
	int number1 = num1;
	static int factorial = 1;
	static int i;

	for (int i = 1; i <= number1; ++i)
	{
		factorial = (factorial * i);
	}
	return (factorial);
}

int mulTwoNum(int num1, int num2)
{
	int number1 = num1;
	int number2 = num2;
	return (number1 * number2);
}
#endif
