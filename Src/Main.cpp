#include "gtest.h"
//#include "C:\Users\tomro\Desktop\VisualStudioProject\UnitTest\UnitTestGtestLib\gtest\gtest.h"
#define STAND_ALONE

int main(int argc, char** argv) {
	
		::testing::InitGoogleTest(&argc, argv);
		return RUN_ALL_TESTS();
}














#ifdef STAND_ALONE
// This will get you up and going 
//The only function: simply multiplies two numbers
double timesTwo(double x, double y) {
	return x * y;
}

char const* array()
{
	return "Hello world!";
}


/* Unit Test section */
TEST(multiplytwonum, iscorrect)
{
	EXPECT_EQ(4, timesTwo(2, 2));
	EXPECT_EQ(6, timesTwo(2, 3));
	EXPECT_EQ(-20, timesTwo(-2, 10));
	EXPECT_NE(21, timesTwo(2, 10)); // not equal

}

TEST(stingtest, iscorrect)
{
	EXPECT_STREQ("Hello world!", array()); // pass
	EXPECT_STREQ("Hello xworld!", array()); // fail
}

#endif