
#ifndef MATH_H
#define MATH_H

class Math;
#include "MathUnitTest.h"

class Math {
private:

	// These are not used at the moment
	int num1;  
	int num2;

public:
	// Add two numbers
	int addTwoNum(int number1, int number2);

	// Sub two numbers
	int subTwoNum(int number1, int number2);

	// Div two numbers
	int divTwoNum(int number1, int number2);

	// Multi two numbers
	int mulTwoNum(int number1, int number2);

	// Factorial
	int factNum(int number1);

	//void setNum(int number1, int number2);

	//int getNum1(void);

	//int getNum2(void);

};


// Traditional "C" approach 
//extern int factNum(int num1);
//extern int mulTwoNum(int num1, int num2);
#endif

