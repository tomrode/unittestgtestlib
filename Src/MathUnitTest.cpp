
#include "Math.h"

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void MathTest::SetUp() {
	math_ = new Math();
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void MathTest::TearDown() {
	delete math_;
}

// Methods to use from your class to test
int MathTest::addTwoNum(int number1, int number2)
{
	return (number1 + number2);
}

int MathTest::subTwoNum(int number1, int number2)
{
	return (number1 - number2);
}

int MathTest::divTwoNum(int number1, int number2)
{
	return (number1 / number2);
}
int MathTest::mulTwoNum(int number1, int number2)
{
	return (number1 * number2);
}

int MathTest::factNum(int number1)
{
	if (number1 == 0)
		return 1;
	else
		return(number1 * factNum(number1 - 1));
}

//Unit Test Area
TEST_F(MathTest, addtwonum)
{
	EXPECT_EQ(2, addTwoNum(1, 1));
	EXPECT_EQ(12, addTwoNum(2, 10));
	EXPECT_EQ(20, addTwoNum(10, 10));
	EXPECT_EQ(100, addTwoNum(50, 50));
}

TEST_F(MathTest, subtwonum)
{
	EXPECT_EQ(9, subTwoNum(10, 1));
	EXPECT_EQ(8, subTwoNum(9, 1));
}

TEST_F(MathTest, factorialnum)
{
	EXPECT_EQ(1, factNum(0));
	EXPECT_EQ(1, factNum(1));
	EXPECT_EQ(2, factNum(2));
	EXPECT_EQ(6, factNum(3));
	EXPECT_EQ(24, factNum(4));
}


#if C_APPROACH
TEST(multiplytwonum, iscorrect)
{
	EXPECT_EQ(4, mulTwoNum(2, 2));
	EXPECT_EQ(6, mulTwoNum(2, 3));
	EXPECT_EQ(20, mulTwoNum(2, 10));
	EXPECT_EQ(21, mulTwoNum(2, 10));

}

TEST(factorialTest, isFactorial)
{
	EXPECT_EQ(1, factNum(0));
	EXPECT_EQ(1, factNum(1));
	EXPECT_EQ(2, factNum(2));
	EXPECT_EQ(6, factNum(3));
	//EXPECT_EQ(24, factNum(4));
	//EXPECT_EQ(24, factNum(5));  // Should fail
}
#endif
